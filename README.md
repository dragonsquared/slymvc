#slyMVC#

##Overview##
A light-weight PHP 5.4 based MVC.

##Resources##
1. Development Board: [https://trello.com/b/ycaQp0Vd/slymvc](https://trello.com/b/ycaQp0Vd/slymvc)
2. BitBucket : [https://bitbucket.org/dragonsquared/slymvc](https://bitbucket.org/dragonsquared/slymvc)
3. Errors / Requests : [https://bitbucket.org/dragonsquared/slymvc/issues](Issues)

##Version History##
1. 0.1 - 2014-06-10

##Requirements##
1. PHP 5.4+
2. Apache2 mod_rewrite enabled
3. Composer : [https://getcomposer.org/]

##Usage##
1. Clone the repository and navigate to /path/to/clone/system and run "composer install"
2. Change the permissions to 777 for the following directories:
 	* /path/to/clone/application/cache

##Documentation##
Beta edition of documentation is being made available for the moment at http://docs.slymvc.com