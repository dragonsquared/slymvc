<?php

ini_set('short_open_tag','on');

if (version_compare(phpversion(), '5.4', '<') ) {
    echo 'Your version of PHP ' . phpversion() . ' is too low. Please upgrade to at least PHP 5.4 <a href="http://php.net/downloads.php" target="_blank">Click Here</a>';
    exit;
}

/**
 * define various paths for use in the application
 */

define('BASEPATH', __DIR__);
define('APP_FOLDER', BASEPATH . '/application/');
define('SYS_FOLDER', BASEPATH . '/system/');

/**
 * Test to see if composer packages have been installed
 */

#if ( !file_exists(SYS_FOLDER . 'vendor/autoload.php') ) {

    /**
     * Test for composer being installed
     */

#    include_once SYS_FOLDER . 'system_packages/Install.php';

#    $install = new \SysPack\Install();

#    $install->set_perms();

#    if ( $_GET ) {
#        $install->update_test();
#    }

#    $install->test_for_composer();

#}

define('start_load', microtime());

session_cache_limiter(null);
session_start();

/**
 * Vendor packages have been stripped of unneeded documentation and files for efficiency
 * If you want to view the documentation, open the file system/vendor/composer/autoload_namespaces.php
 * Then go to packagist.com and search for them to get their documentation
 */

require_once('system/vendor/autoload.php');

/**
 * Load the core file
 */

require_once SYS_FOLDER . 'core_system/Sly_Core.php';

/**
 * Initialize the core file
 */

$app = new \SlyCore\Sly_Core();

define('base_url', $app->base_url());
define('full_url', $app->full_url());

/**
 * These files must be loaded in this method to avoid looping
 */

$app->load->core('AppController');
$app->load->core('AppModel');

/**
 * Run the application
 */

$app->run();