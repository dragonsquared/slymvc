<?php

namespace SlyLibraries;

use Config\Config;
use Swift_MailTransport;
use Swift_Mailer;
use Swift_Message;

include APP_FOLDER . 'mail_templates/Templates.php';

/**
 * Class Sly_Mailer
 *
 * Email creation class
 *
 * @package     slyMVC
 * @subpackage  SlyLibraries
 * @author      slyMVC Dev Team
 * @link        http://docs.slymvc.com
 */
class Sly_Mailer {

    protected $transport;
    protected $mailer;
    protected $message;

    protected $set_sender;
    protected $set_return_path;

    /**
     * Constructor
     *
     * Initialize Configuration with settings in config file and from this, determine
     * transport method and instantiate SwiftMailer object with these settings
     */
    public function __construct() {
        $cfg                = new Config();
        $settings           = $cfg->cfg();
        $this->settings     = $settings;

        switch($settings['mail_type']) {
            case 'mail':
                $this->transport = Swift_MailTransport::newInstance();
                break;
            case 'smtp':
                $this->transport = \Swift_SmtpTransport::newInstance($settings['mail_host'], $settings['mail_port'])
                    ->setUsername($settings['mail_username'])
                    ->setPassword($settings['mail_password']);
                break;
            default:
                $this->transport = Swift_MailTransport::newInstance();
        }

        $this->mailer       = Swift_Mailer::newInstance($this->transport);
        $this->message      = Swift_Message::newInstance();
    }

    // Chained Methods

    /**
     * Message
     *
     * Create the message
     *
     * @return $this
     */
    public function message() {
        $this->message      = Swift_Message::newInstance();
        return $this;
    }

    /**
     * Sender
     *
     * Specifies the address of the person who physically sent the message (higher precedence than From)
     *
     * @param string $sender
     * @return $this
     */
    public function sender($sender) {
        $this->message->setSender($sender);
        return $this;
    }

    /**
     * From
     *
     * Specifies the address of the person who the message is from. This can be multiple addresses if
     * multiple people wrote the message.
     *
     * @param string $from
     * @return $this
     */
    public function from($from) {
        $this->message->setFrom($from);
        return $this;
    }

    /**
     * Reply_To
     *
     * Specifies the address where replies are sent to
     *
     * @param string $reply_to
     * @return $this
     */
    public function reply_to($reply_to) {
        $this->message->setReplyTo($reply_to);
        return $this;
    }

    /**
     * To
     *
     * Specifies the addresses of the intended recipients
     *
     * @param string $to
     * @return $this
     */
    public function to($to) {
        $this->message->setTo($to);
        return $this;
    }

    /**
     * Subject
     *
     * Specifies the subject line that is displayed in the recipients' mail client
     *
     * @param string $subject
     * @return $this
     */
    public function subject($subject) {
        $this->message->setSubject($subject);
        return $this;
    }

    /**
     * Body
     *
     * adds message body content to object instance
     *
     * @param $body
     * @return $this
     */
    public function body($body) {
        $this->message->setBody($body, 'text/plain');
        return $this;
    }

    /**
     * HTML
     *
     * Sets message format to html
     *
     * @param $html
     * @return $this
     */
    public function html($html) {
        $this->message->setBody($html, 'text/html');
        return $this;
    }

    /**
     * Template
     *
     * Sets the template the email will use, and allows parameters to be passed in.
     *
     * @param string $template_name
     * @param null|array $data
     * @return $this
     */
    public function template($template_name, $data=null) {
        $templates  = new \Templates();
        $template   = $templates->$template_name($data);
        $this->message->setBody($template, 'text/html');
        return $this;
    }

    /**
     * CC
     *
     * Specifies the addresses of recipients who will be copied in on the message
     *
     * @param $cc
     * @return $this
     */
    public function cc($cc) {
        $this->message->setCc($cc);
        return $this;
    }

    /**
     * BCC
     *
     * Specifies the addresses of recipients who the message will be blind-copied to.
     * Other recipients will not be aware of these copies.
     *
     * @param $bcc
     * @return $this
     */
    public function bcc($bcc) {
        $this->message->setBcc($bcc);
        return $this;
    }

    /**
     * Attach File
     *
     * Allows files to be attached to email and named
     *
     * @param string|array $file
     * @param string $name
     * @return $this
     */
    public function attach_file($file, $name) {
        if ( is_array($file) ) {
            $keys = array_keys($file);
            if ( !is_numeric($keys[0]) ) {
                foreach ( $file as $k => $v ) {
                    if ( file_exists($v) ) {
                        $this->message->attach(\Swift_Attachment::fromPath($v)->setFilename($k));
                    }
                }
            } else {
                foreach ($file as $f) {
                    if (file_exists($f)) {
                        $this->message->attach(\Swift_Attachment::fromPath($f));
                    }
                }
            }
        } else {
            if ( file_exists($file) ) {
                $attachment = \Swift_Attachment::fromPath($file);
                !$name || $attachment->setFilename($name);
                $this->message->attach($attachment);
            }
        }
        return $this;
    }

    /**
     * Attach URL
     *
     * Attach a file from the specified url
     *
     * @param $url
     * @return $this
     */
    public function attach_url($url) {
        if ( ini_get('allow_url_fopen') ) {
            $this->message->attach(\Swift_Attachment::fromPath($url));
        }
        return $this;
    }

    /**
     * Attach Download
     *
     * The attachment will be displayed within the email viewing window if the mail client knows how to display it.
     *
     * @param $file
     * @return $this
     */
    public function attach_download($file) {
        $this->message->attach(\Swift_Attachment::fromPath($file)->setDisposition('inline'));
        return $this;
    }

    /**
     * Send
     *
     * sends the newly constructed messafe
     *
     * @return int
     */
    public function send() {
        return $this->mailer->send($this->message);
    }

    /**
     * Bounce
     *
     * Specifies where bounces should go
     *
     * @param $bounce
     * @return $this
     */
    public function bounce($bounce) {
        $this->message->setReturnPath($bounce);
        return $this;
    }

    /**
     * Send Text Email
     *
     * Send a simple text format email
     *
     * @param string $from email address
     * @param string $to email address
     * @param string $subject
     * @param string $msg
     * @return int
     */
    public function send_text_email($from, $to, $subject, $msg) {
        $this->message->setFrom($from)->setTo($to)->setSubject($subject)->setBody($msg, 'text/plain');
        return $this->mailer->send($this->message);
    }

} 