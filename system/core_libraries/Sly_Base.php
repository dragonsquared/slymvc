<?php

namespace SlyLibraries;

/**
 * Class Sly_Base
 *
 * Common useful functions
 *
 * @package     slyMVC
 * @subpackage  SlyLibraries
 * @author      slyMVC Dev Team
 * @link        http://docs.slymvc.com
 */
class Sly_Base {

    public function __construct() {

    }

    /**
     * Generate Random Alpha String
     *
     * @param int $length
     * @return string
     */
    public function random_alpha_string($length=8) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $char_arr = str_split($chars);
        $string = '';
        for ( $i=0;$i<$length;$i++ ) {
            $string .= $char_arr[array_rand($char_arr, 1)];
        }
        return $string;
    }

    /**
     * Generate Random Alphanumeric String
     *
     * @param int $length
     * @return string
     */
    public function random_alphanumeric_string($length=8) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $char_arr = str_split($chars);
        $string = '';
        for ( $i=0;$i<$length;$i++ ) {
            $string .= $char_arr[array_rand($char_arr, 1)];
        }
        return $string;
    }

} 