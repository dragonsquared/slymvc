<?php

namespace SlyLibraries;

use SlyCore\Sly_Core;
use Crypt_AES;

/**
 * Class Sly_Encrypt
 *
 * Encryption functions
 *
 * @package     slyMVC
 * @subpackage  SlyLibraries
 * @author      slyMVC Dev Team
 * @link        http://docs.slymvc.com
 */
class Sly_Encrypt extends Sly_Core {

    protected $key;

    /**
     * Constructor
     *
     * encryption key from config file
     */
    public function construct() {
        parent::__construct();
        $this->key = $this->settings['encrypt_key'];
    }

    /**
     * Simple Encode
     *
     * Encodes a string using base64 encoding
     *
     * @param string $string
     * @return string
     */
    public function simple_encode($string) {
		return urlencode(base64_encode($string));
	}

    /**
     * AES Encode
     *
     * Encodes a string using AES encryption
     *
     * @param string $string
     * @return string
     */
    public function encode($string) {
        $aes = new Crypt_AES();

        $aes->setKey($this->key);

        return base64_encode($aes->encrypt($string));
    }

    /**
     * Simple Decode
     *
     * decodes a base64 encoded string
     *
     * @param string $string
     * @return string
     */
    public function simple_decode($string) {
		return base64_decode(urldecode($string));
	}

    /**
     * AES Decode
     *
     * Decodes an AES encrypted string
     *
     * @param string $string
     * @return String
     */
    public function decode($string) {
        $aes = new Crypt_AES();

        $aes->setKey($this->key);

        $string = base64_decode($string);

        return $aes->decrypt($string);
    }

    /**
     * Password Encrypt
     *
     * generates a hash from a password
     *
     * @param string $password
     * @return bool|false|string
     */
    public function password_encrypt($password) {
        return password_hash($password, PASSWORD_DEFAULT, ['cost'=>10]);
    }

    /**
     * Password Verify
     *
     * compares password against hashed password
     *
     * @param string $password
     * @param hash string $stored_password
     * @return bool
     */
    public function password_verify($password, $stored_password) {
        return password_verify($password, $stored_password);
    }

} 