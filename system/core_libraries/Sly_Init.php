<?php

namespace SlyLibraries;


/**
 * Class Sly_Init
 *
 * A class consisting of simple ini configuration file operations
 * @package     slyMVC
 * @subpackage  SlyLibraries
 * @author      slyMVC Dev Team
 * @link        http://docs.slymvc.com
 */
class Sly_Init {

    /**
     * Constructor
     *
     * Set the cache folder to a variable
     */
    public function __construct() {
        $this->cache_folder = APP_FOLDER . 'cache/';
    }

    /**
     * File Exists
     *
     * Returns true/false if the file exists
     *
     * @param string $file
     * @return bool
     */
    public function exists($file) {
        return (bool) file_exists($file);
    }

    /**
     * Read INI
     *
     * Looks in the cache directory for the ini file; if it exists, return it as an
     * associative array, or else return null
     *
     * @param string $filename
     * @return array|null
     */
    public function read_ini($filename) {
        $file = $this->cache_folder . $filename;
        return $this->exists($file) ? parse_ini_file($file) : null;
    }

    /**
     * Write INI File
     *
     * Generate an ini file using the passed in data
     *
     * @param string $filename
     * @param array $data
     * @return bool
     */
    public function write_ini_file($filename, $data) {

        $file = $this->cache_folder . $filename;

        if ( count($data) > 1 ) {
            foreach ( $data as $k => $v ) {
                $line = "[" . $k . "]\n";
                file_put_contents($file, $line, FILE_APPEND | LOCK_EX);
                foreach ( $v as $k1 => $v1 ) {
                    $line = "$k" . "[$k1] = \"$v1\"\n";
                    file_put_contents($file, $line, FILE_APPEND | LOCK_EX);
                }
                $final_line = "\n";
                file_put_contents($file, $final_line, FILE_APPEND | LOCK_EX);
            }
        } else {
            foreach ( $data as $k => $v ) {
                $line = "[" . $k . "]\n";
                file_put_contents($file, $line, FILE_APPEND | LOCK_EX);
                foreach ( $v as $k1 => $v1 ) {
                    $line = "$k" . "[$k1] = \"$v1\"\n";
                    file_put_contents($file, $line, FILE_APPEND | LOCK_EX);
                }
            }
        }
        return $this->exists($file);
    }

    /**
     * Add to INI
     *
     * Add data to an ini configuration file
     * @param string $filename
     * @param array $data
     * @return bool
     */
    public function add_to_ini($filename, $data) {
        $file = $this->cache_folder . $filename;
        $curr = parse_ini_file($file);
        foreach ( $data as $k => $v ) {
            if ( in_array($k, array_keys($data)) ) {
                foreach ( $v as $k2 => $v2 ) {
                    $curr[$k][$k2] = $v2;
                }
            } else {
                foreach ( $v as $k2 => $v2 ) {
                    $curr[$k][$k2] = $v2;
                }
            }
        }
        $this->remove_ini($filename);
        return $this->write_ini_file($filename, $curr);
    }

    /**
     * Edit INI
     *
     * Edit a configuration file
     *
     * @param string $filename
     * @param int $key
     * @param string $value
     * @return bool
     */
    public function edit_ini($filename, $key, $value) {
        $file = $this->cache_folder . $filename;

        $curr = parse_ini_file($file);

        if ( count($key) > 1 ) {
            $curr[$key[0]][$key[1]] = $value;
        } else {
            $curr[$key] = $value;
        }

        $this->remove_ini($filename);
        return $this->write_ini_file($filename, $curr);
    }

    /**
     * Remove INI
     *
     * Delete a configuration file
     *
     * @param string $filename
     */
    public function remove_ini($filename) {
        $file = $this->cache_folder . $filename;
        unlink($file);
    }

} 