<?php

namespace SlyLibraries;

/**
 * Class Sly_Cache
 *
 * Cache Library which handles basic caching functionality
 *
 * @package     slyMVC
 * @subpackage  SlyLibraries
 * @author      slyMVC Dev Team
 * @link        http://docs.slymvc.com
 */
class Sly_Cache {

    protected $cache_folder;

    /**
     * Constructor
     *
     * Load in the cache folder value from the config file, and instantiate the exception library
     *
     * @param $settings
     */
    public function __construct($settings) {

        $this->class_settings = $settings;

        $this->cache_folder = $settings['cache_folder'];

        $this->exception = new Sly_Exception($settings['exceptions']);

    }

    /**
     * Cache Directory
     *
     * return the cache directory including the cache filename
     *
     * @param null|string $filename
     * @return string
     */
    public function cache_dir($filename=null) {
        return $this->cache_folder . '/' . $filename;
    }

    /**
     * Test Cache Folder
     *
     * Checks the permissions of the cache folder, and depending upon the silent parameter,
     * either throw an exception or return a boolean
     *
     * @param null|bool $silent
     * @return bool|string
     */
    public function test_cache_folder($silent=null) {
        $perms  = $this->check_permissions($this->cache_folder);
        $data['title'] = 'Cache Folder Test';
        if ( $perms != 777 ) {
            $this->throw_except($this->cache_folder);
        }
        if ( $silent ) {
            return (bool) $perms = 777;
        }
    }

    /**
     * Throw Exception
     *
     * Throw a directory permissions exception on the cache director in question, prompting the developer to change the
     * permissions to Read/Write/Execute
     *
     * @param string $dir
     */
    public function throw_except($dir) {
        $data['title'] = 'Error';
        $data['msg'] = 'Please update the permissions for : ' . $dir . ' to 777';
        include(dirname(__DIR__) . '/system_pages/template.php');
        exit;
    }

    /**
     * Check permissions
     *
     * Return the file permissions information
     *
     * @param string $file
     * @return int
     */
    public function check_permissions($file) {
        return (int) substr(sprintf('%o', fileperms($file)), -4);
    }

    /**
     * Write to Cache
     *
     * If the cache file exists and is writable, write the contents into the cache file
     *
     * @param $filename
     * @param $contents
     * @param int $valid minutes
     */
    public function write($filename, $contents, $valid=null) {
        $filename = $this->cache_folder . '/' . $filename;
        if ( file_exists($filename) ) {
            if ( !is_writable($filename) ) {
                $this->exception->run('File not writable : ' . $filename);
                exit;
            }
        }
        $o_name = $filename;
        $filename = @fopen($filename, 'w');
        if ( !file_exists($o_name)) {
            $this->exception->run('File could not be created : ' . $o_name);
            exit;
        }
        if ( $valid ) {
            $valid_time = json_encode(['valid_til'=>time() + 60*$valid]) . "\n";
            fwrite($filename, $valid_time);
        }
        if ( is_array($contents) ) {
            $contents = json_encode($contents);
        }
        fwrite($filename, $contents);
        fclose($filename);
    }

    /**
     * Read Cache File
     *
     * Read the contents of the specified cache file and return them as one of the following types:
     * pretty, json, object, or array. The default is set in /application/config/config.php
     *
     * @param $filename
     * @param type $return
     * @return mixed|null|string
     */
    public function read($filename, $return=null) {
        $this->test_cache_folder(1) || $this->test_cache_folder();
        $filename = $this->cache_dir($filename);
        if ( !file_exists($filename) ) {
            $this->exception->run('File not found : ' . $filename);
            exit;
        }
        $file = file_get_contents($filename);
        $stamp = explode("\n", $file)[0];
        if ( strpos($stamp, 'valid') !== false ) {
            $stamp_arr = json_decode($stamp, true);
            if ( is_array($stamp_arr) ) {
                if ( array_keys($stamp_arr)[0] == 'valid_til' ) {
                    if ( time() > $stamp_arr['valid_til'] ) {
                        $this->del($filename);
                        return null;
                    }
                    $file = str_replace( $stamp, "", $file);
                }
            }
        }
        if ( !$return ) {
            $return = $this->class_settings['cache_default_ret'];
        }
        $contents = $file;
        switch($return) {
            case 'pretty':
                $data = $contents;
                break;
            case 'json':
                $data = json_encode(json_decode($contents, true));
                break;
            case 'object':
                $data = json_decode($contents);
                break;
            case 'array':
                $data = json_decode($contents, true);
                break;
            default:
                $data = $contents;
        }
        return $data;
    }

    /**
     * Delete Cache File
     *
     * Delete the cache file if it exists in the cache directory
     *
     * @param string $filename
     * @return bool
     */
    public function del($filename) {
        $filename = $this->cache_dir($filename);
        if ( file_exists($filename) ) {
            unlink($filename);
        }
        if ( file_exists($filename) ) {
            $this->exception->run('There was an error removing : ' . $filename);
            exit;
        }
        return true;
    }

    /**
     * Write Template
     *
     * Create a template from which cache files can be written
     *
     * @param $contents
     * @param int $valid minutes to keep cache file valid
     * @return string
     */
    public function write_template($contents, $valid=null) {
        $str= "<?\n";
        if ( $valid ) {
            $str .= '$valid_til=' . (time() + 60*$valid) . ";\n";
        }
        $str .= '$data_array=\'' . $contents . '\';';
        return $str;
    }

} 