<?php

namespace SlyLibraries;

use SysPack\PHPThumb\GD;

/**
 * Class Sly_Images
 *
 * Image manipulation
 *
 * @package     slyMVC
 * @subpackage  SlyLibraries
 * @author      slyMVC Dev Team
 * @link        http://docs.slymvc.com
 */
class Sly_Images {

    public function __construct() {

    }

    /**
     * Exists
     *
     * Returns true/false if the file exists
     *
     * @param string $filename
     * @return bool
     */
    public function exists($filename) {
        return (bool) file_exists($filename);
    }

    /**
     * Get Image
     *
     * @param string $filename
     */
    public function get_image($filename) {
        $ext    =   explode(".", $filename);
        $ext    =   $ext[count($ext)-1];
        header("Content-type: image/$ext");
        echo file_get_contents($filename);
    }

    /**
     * Resize's image
     *
     * Defaults to 50% of original size
     *
     * @param string $filename
     * @param int $size
     * @param bool $save
     * @return GD
     */
    public function resize($filename, $size=50, $save=false) {
        $o_file =   $filename;
        $x_file =   explode(".", $filename);
        $t_file =   $x_file[0] . "_" . $size . "." . $x_file[1];
        !$this->exists($t_file) || $this->get_image($t_file);
        $img    =   new GD($o_file);
        $img->resizePercent($size);
        !$save || $img->save($t_file);
        return $img;
    }

    /**
     * Creates a thumbnail from the center of an image
     *
     * Defaults to 100px * 100px
     *
     * @param $filename
     * @param int $size
     * @param bool $save
     * @return GD
     */
    public function thumb($filename, $size=100, $save=false) {
        $o_file =   $filename;
        $x_file =   explode(".", $filename);
        $t_file =   $x_file[0] . "_" . $size . "_thumb." . $x_file[1];
        !$this->exists($t_file) || $this->get_image($t_file);
        $img    =   new GD($o_file);
        $img->adaptiveResize($size, $size);
        !$save || $img->save($t_file);
        return $img;
    }

    /**
     * Rotate Image
     *
     *  Rotates an image on the fly and allows this newly rotated image to be saved
     * @param $filename
     * @param int $deg  rotation
     * @param bool $save
     * @return GD
     */
    public function rotate($filename, $deg=90, $save=false) {
        $o_file =   $filename;
        $x_file =   explode(".", $filename);
        $t_file =   $x_file[0] . "_rot_" . $deg . "." . $x_file[1];
        !$this->exists($t_file) || $this->get_image($t_file);
        $img    =   new GD($o_file);
        $img->rotateImageNDegrees($deg);
        !$save || $img->save($t_file);
        return $img;
    }

}