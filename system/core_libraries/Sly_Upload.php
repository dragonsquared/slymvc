<?php

namespace SlyLibraries;

/**
 * Class Sly_Upload
 *
 * File Upload class
 *
 * @package     slyMVC
 * @subpackage  SlyLibraries
 * @author      slyMVC Dev Team
 * @link        http://docs.slymvc.com
 */
class Sly_Upload {

    protected $max_size;
    protected $max_height;
    protected $max_width;
    protected $upload_path;
    protected $allowed_types;
    protected $overwrite;
    protected $filename;
    protected $encrypt_name;
    protected $remove_spaces;
    protected $error;

    /**
     * Constructor
     *
     * @param array $initial_settings
     */
    public function __construct(array $initial_settings=[]) {
        $this->encode   =   new sly_Encrypt();
        if ( count($initial_settings) == 0 ) {
            die("There is an issue with your upload settings");
        }
        foreach ( $initial_settings as $k => $v ) {
            $this->$k = $v;
        }
    }

    /**
     * @param $image
     * @param null $settings
     * @return array|bool
     */
    public function image($image, $settings=null) {
        $this->update_settings($settings);
        $file   =   $_FILES[$image];
        $name   =   $file['name'];
        $t_name =   $file['tmp_name'];
        if ( $file['error'] !== UPLOAD_ERR_OK ) {
            return ['error'=>'There was an error uploading the file. Error Code : ' . $file['error']];
        }
        $img_size   =   $file['size']/1024;
        if ( $img_size > $this->max_size ) {
            $this->add_error("Image size too large - max : $this->max_size - image : $img_size");
        }
        $type   =   $this->get_file_type($t_name, 1);
        if ( $type !== $file['type'] ) {
            $this->add_error("File types do not match : received - $file[type] : actual - $type");
        }
        if ( !$this->allowed_type($t_name) ) {
            $this->add_error("File type not allowed : received - $ext");
        }
        if ( explode('/', $type)[0] == 'image' ) {
            $dimensions = $this->get_img_dim($t_name);
            $width = $dimensions['width'];
            $height = $dimensions['height'];
            if ($width > $this->max_width) {
                $this->add_error("Image too width - max : $this->max_width - img : $width");
            }
            if ($height > $this->max_height) {
                $this->add_error("Image too tall - max : $this->max_height - img : $height");
            }
        }
        $response   =   true;
        if ( $this->error() ) {
            $response   =   ['error'=>$this->error];
            unlink($t_name);
        } else {
            $name   =   $this->remove_spaces ? $this->remove_spaces($name) : $name;
            $name   =   $this->encrypt_name ? $this->encrypt_name($name) : $name;
            if ( $this->encrypt_name ) {
                $response   =   ['encrypted_name'=>$name];
            }
            $full   =   $this->upload_path . $name;
            if ( $this->exists($full) ) {
                if ( !$this->overwrite ) {
                    $this->add_error("File already exists.");
                    return ["error"=>$this->error];
                }
            }
            $move   =   $this->move_file($t_name, $full);
            if ( !is_bool($move) ) {
                return ["error"=>$move];
            }
        }
        return $response;
    }

    /**
     * @param $filename
     * @param null $settings
     */
    public function text_file($filename, $settings=null) {
        $this->update_settings($settings);
    }

    /**
     * @param $filename
     * @param null $settings
     * @return array|bool
     */
    public function file($filename, $settings=null) {
        $this->update_settings($settings);
        $file   =   $_FILES[$filename];
        if ( $file['error'] !== UPLOAD_ERR_OK ) {
            return ['error'=>'There was an error uploading the file. Error Code : ' . $file['error']];
        }
        $t_name =   $file['tmp_name'];
        $name   =   $file['name'];
        $size   =   $this->get_size($t_name);
        $type   =   $this->get_file_type($t_name, 1);
        if ( $type !== $file['type'] ) {
            $this->add_error("File types do not match : received - $file[type] : actual - $type");
        }
        $ext    =   explode('/', $type)[1];
        if ( !$this->allowed_type($ext) ) {
            $this->add_error("File type not allowed : received - $ext");
        }
        if ( $size > $this->max_size ) {
            $this->add_error("File exceeds max size : received - $size : max - $this->max_size");
        }
        if ( strpos($type, 'image') !== false ) {
            $dims = $this->get_img_dim($t_name);
            $width = $dims['width'];
            $height = $dims['height'];
            if ($this->comp_size($width, $this->max_width)) {
                $this->add_error("File width exceeds max width : received - $width : max - $this->max_width");
            }
            if ($this->comp_size($height, $this->max_height)) {
                $this->add_error("File height exceeds max height : received - $height : max - $this->max_height");
            }
        }
        $response   =   true;
        if ( $this->error() ) {
            $response   =   ['error'=>$this->error];
            unlink($t_name);
        } else {
            $name   =   $this->remove_spaces ? $this->remove_spaces($name) : $name;
            $name   =   $this->encrypt_name ? $this->encrypt_name($name) : $name;
            if ( $this->encrypt_name ) {
                $response   =   ['encrypted_name'=>$name];
            }
            $full   =   $this->upload_path . $name;
            if ( $this->exists($full) ) {
                if ( !$this->overwrite ) {
                    $this->add_error("File already exists.");
                    return ["error"=>$this->error];
                }
            }
            $move   =   $this->move_file($t_name, $full);
            if ( !is_bool($move) ) {
                return ["error"=>$move];
            }
        }
        return $response;
    }

    /**
     * Allowed Types
     *
     * Checks if the file is within the array of allowed file types
     * The default set in application/config/config.php are ['jpg', 'jpeg', 'gif', 'png']
     * Returns True/False
     *
     * @param string $file
     * @return bool
     */
    protected function allowed_type($file) {
        $ext    =   $this->get_file_type($file, 1)[1];
        return !in_array($ext, $this->allowed_types) ? true : false;
    }

    /**
     * @param null $settings
     * @return array|bool
     */
    protected function update_settings($settings=null) {
        if ( $settings ) {
            if ( is_array($settings) ) {
                foreach ( $settings as $k => $v ) {
                    $this->$k = $v;
                }
                return true;
            }
            return ['error'=>'Settings passed to function must be in array format'];
        }
        return true;
    }

    /**
     * Move File
     *
     * Attempt to move the file. If the file cannot be moved, then an error message is thrown. If the file is moved
     * the method determines if the new file exists at the destination, and returns boolean.
     *
     * @param string $orig
     * @param string $dest
     * @return bool|string
     */
    private function move_file($orig, $dest) {
        try {
            if ( !move_uploaded_file($orig, $dest) ) {
                throw new \Exception('Could not move file');
            }
            chmod($dest, 0777);
        } catch (\Exception $e ) {
            return $e->getMessage();
        }
        return $this->exists($dest) ? true : false;
    }

    /**
     * @param $name
     * @return string
     */
    private function encrypt_name($name) {
        $name   =   explode('.', $name);
        $ext    =   $name[count($name)-1];
        $arr    =   array_pop($name);
        $name   =   @implode('_', $arr);
        $name   =   $this->encode->encrypt($name);
        return $name . "." . $ext;
    }

    /**
     * Compare Size
     *
     * Calculates whether the first int passed in is larger than the second, then returns boolean
     *
     * @param int $int_1
     * @param int $int_2
     * @return bool
     */
    private function comp_size($int_1, $int_2) {
        if ( $int_2 !== 0 ) {
            if ( $int_1 > $int_2 ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get Size
     *
     * Returns the size of the file in KB, with a default rounding of decimal to nearest 100ths
     *
     * @param string $filename
     * @param int $precision default 2
     * @return float
     */
    public function get_size($filename, $precision=2) {
        // Returns file size in KB
        $size   = round(filesize($filename) / 1024, $precision);
        return $size;
    }

    /**
     * Get File Type
     *
     * Returns the file extension;
     * If the optional second parameter is set to TRUE, the mime type of the file will be returned
     *
     * @param string $filename
     * @param bool $ret default null
     * @return mixed
     */
    public function get_file_type($filename, $ret=null) {
        $finfo  =   finfo_open(FILEINFO_MIME_TYPE);
        $type   =   finfo_file($finfo, $filename);
        $ext    =   explode('/', $type)[0];
        return $ret ? $type : $ext;
    }

    /**
     * Get Image Dimensions
     *
     * Returns the dimensions, type, and attributes of the image passed in as an associative array
     *
     * @param string $filename
     * @return array
     */
    public function get_img_dim($filename) {
        list($width, $height, $type, $attr) = getimagesize($filename);
        $dims =   [
            'width' =>$width,
            'height'=>$height,
            'type'  =>$type,
            'attr'  =>$attr
        ];
        return $dims;
    }

    /**
     * File Info
     *
     * Returns the path information and permissions for a file
     *
     * @param string $filename
     * @return mixed
     */
    public function file_info($filename) {
        return pathinfo($filename);
    }

    /**
     * Remove File
     *
     * Deletes a file and returns success as true/false
     *
     * @param string $filename
     * @return bool
     */
    public function remove_file($filename) {
        unlink($filename);
        return $this->exists($filename) ? false : true;
    }

    /**
     * File Exists
     *
     * Returns true/false if the file exists
     *
     * @param strinf $filename
     * @return bool
     */
    public function exists($filename) {
        return (bool) file_exists($filename);
    }

    /**
     * Add Error to Error Array
     *
     * Adds the passed in error message to the error array
     *
     * @param string $error_message
     */
    public function add_error($error_message) {
        $this->error[]  =   $error_message;
    }

    /**
     * Error
     *
     * If any errors exist in the error array, display them, otherwise return null
     *
     * @return null
     */
    public function error() {
        return count($this->error) > 0 ? $this->error : null;
    }

    /**
     * Remove Spaces
     *
     * Replaces the spaces of a file name with underscores
     *
     * @param string $filename
     * @return mixed
     */
    public function remove_spaces($filename) {
        return str_replace(' ', '_', $filename);
    }

}