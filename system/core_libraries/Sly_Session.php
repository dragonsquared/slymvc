<?php

namespace SlyLibraries;

/**
 * Class Sly_Session
 *
 * Basic session manipulation
 *
 * @package     slyMVC
 * @subpackage  SlyLibraries
 * @author      slyMVC Dev Team
 * @link        http://docs.slymvc.com
 */
class Sly_Session {

    /**
     * Constructor
     *
     * Checks if the browser contains a session id cookie, and if so, set the php session id from it.
     */
    public function __construct() {
		if ( isset($_COOKIE['PHPSESSID']) ) {
			$this->set('php_session_id', $_COOKIE['PHPSESSID']);
		}
	}

    /**
     * Set
     *
     * @param string $name
     * @param mixed $value
     * @return bool
     */
    public function set($name, $value) {
		$_SESSION[$name] = $value;
		return (bool) $this->get($name) == $value;
	}

    /**
     * Flash
     *
     * Create a session flash, which lasts for a single request.
     *
     * @param string $name
     * @param null $value
     * @return bool|null
     */
    public function flash($name, $value=null) {
		if ( $value ) {
			$_SESSION['flash_' . $name] = $value;
			return (bool) $this->get('flash_' . $name) == $value;
		}
		$flash = $this->get('flash_' . $name);
		if ( $flash ) {
			$this->rem('flash_' . $name);
			return $flash;
		}
		return null;
	}

    /**
     * Get
     *
     * Get the passed in session variable by key
     *
     * @param string $name
     * @return null
     */
    public function get($name) {
		return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
	}

    /**
     * Remove
     *
     * Removes the passed in session key
     *
     * @param string $name
     * @return bool
     */
    public function rem($name) {
		unset($_SESSION[$name]);
		return (bool) isset($_SESSION[$name]);
	}

    /**
     * All
     *
     * Return all session variables
     *
     * @return mixed
     */
    public function all() {
		return $_SESSION;
	}

    /**
     * End
     *
     * Destroy the current session
     */
    public function end() {
		session_destroy();
	}
	
}