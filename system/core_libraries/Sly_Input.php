<?php

namespace SlyLibraries;

/**
 * Class Sly_Input
 *
 * Input cleansing and sanitizing
 *
 * @package     slyMVC
 * @subpackage  SlyLibraries
 * @author      slyMVC Dev Team
 * @link        http://docs.slymvc.com
 */
class Sly_Input {

    public function __construct() {

    }

    /**
     * Clean Input
     *
     * Trim input, removing script, style, and php tags
     *
     * @param string $input
     * @return mixed
     */
    private function cleanInput($input) {

        $input = trim($input);

        $search =   [
            '@<script[^>]*?>.*?</script>@si',
            '@<[\/\!]*?[^<>]*?>@si',
            '@<style[^>]*?>.*?</style>@siU',
            '@<![\s\S]*?--[ \t\n\r]*>@'
        ];

        $output =   preg_replace($search, '', $input);
        return $output;
    }

    /**
     * Sanitize Input
     *
     * Escape and strip slashes from input; accepts strings and arrays
     *
     * @param string/array $input
     * @return mixed
     */
    private function sanitize($input) {
        if (is_array($input)) {
            foreach ( $input as $var => $val ) {
                $output[$var]   =   $this->sanitize($val);
            }
        }
        else {
            if (get_magic_quotes_gpc()) {
                $input  =   stripslashes($input);
            }
            $input  =   $this->cleanInput($input);
            $output =   $input;
        }
        return $output;
    }

    /**
     * Get Input
     *
     * Sanitize http get variable
     *
     * @param null $name
     * @return mixed|null
     */
    public function get($name=null) {
        return isset($_GET[$name]) ? $this->sanitize($_GET[$name]) : null;
    }

    /**
     * Post Input
     *
     * Sanitize http post variable
     *
     * @param null $name
     * @return mixed|null
     */
    public function post($name=null) {
        return isset($_POST[$name]) ? $this->sanitize($_POST[$name]) : null;
    }

    /**
     * Raw Input
     *
     * Use at your own risk! http post variable without any sanitizing or cleansing
     *
     * @param null $name
     * @return mixed|null
     */
    public function raw($name=null) {
        return isset($_POST[$name]) ? $_POST[$name] : null;
    }
} 