<?php

namespace SlyLibraries;

/**
 * Class Sly_Files
 *
 * Simple file operation methods
 *
 * @package     slyMVC
 * @subpackage  SlyLibraries
 * @author      slyMVC Dev Team
 * @link        http://docs.slymvc.com
 */
class Sly_Files {

    /**
     * Constructor
     *
     * Instantiates an instance of the Sly_Exception Library to utilize exceptions within file operations
     */
    public function __construct() {
		$this->except = new Sly_Exception();
    }

    /**
     * Get File Info
     *
     * Returns the path information and permissions for a file
     *
     * @param string $filename
     * @return mixed
     */
    public function get_file_info($filename) {
        return pathinfo($filename);
    }

    /**
     * File Exists
     *
     * Returns true/false if the file exists
     *
     * @param string $filename
     * @return bool
     */
    public function exists($filename) {
        return (bool) file_exists($filename);
    }

    /**
     * Path Exists
     *
     * Checks whether path exists
     *
     * @param string $path
     * @return bool
     */
    public function path_exists($path) {
		return (bool) is_dir($path);
	}

    /**
     * Create Path Recursive
     *
     * Recursively create a path, generating parent paths if they do not exist and sets the
     * default dir permissions to Read Write Execute
     *
     * @param string $path
     * @return bool
     */
    public function create_path_recursive($path) {
		if ( !$this->path_exists($path) ) {
			try {
				mkdir($path, 0777, true);
				return true;
			} catch (\Exception $e) {
				$this->except->render_exception($e);
			}
		}
	}
	
} 