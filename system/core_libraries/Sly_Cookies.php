<?php

namespace SlyLibraries;

/**
 * Class Sly_Cookies
 *
 * Cookie Handler with basic get, set, and remove functionality
 *
 * @package     slyMVC
 * @subpackage  SlyLibraries
 * @author      slyMVC Dev Team
 * @link        http://docs.slymvc.com
 */
class Sly_Cookies {

    /**
     * Constructor
     *
     * Instantiates an instance of the Sly_Encrypt Library to utilize encryption within cookies
     */
    public function __construct() {
        $this->encode = new Sly_Encrypt();
    }

    /**
     * Set Cookie
     *
     * Generate a new cookie
     *
     * @param array $arr  cookie settings
     * @return bool
     */
    public function set($arr) {
        $name   =   $arr['name'];
        $value  =   $arr['value'];
        $encrypt=   isset($arr['encrypt']) ? 1 : null;
        $exp    =   isset($arr['exp']) ? time() + $arr['exp'] : time() + 3600;
        $path   =   isset($arr['path']) ? $arr['path'] : '/';
        $domain =   isset($arr['domain']) ? $arr['domain'] : null;
        $secure =   isset($arr['secure']) ? $arr['secure'] : null;
        $http   =   isset($arr['httponly']) ? $arr['httponly'] : null;
        $value  =   $encrypt ? $this->encode->encode($value) : $value;
        $set    =   setcookie($name, $value, $exp, $path, $domain, $secure, $http);
        return $set;
    }

    /**
     * Get Cookie
     *
     * Returns cookie if it exists
     * @param string $name
     * @param null|bool $encrypted
     * @return null|String
     */
    public function get($name, $encrypted=null) {
        if ( isset($_COOKIE[$name]) ) {
            $value  =   $_COOKIE[$name];
            if ( $encrypted ) {
                $value  =   $this->encode->decode($value);
            }
            return $value;
        }
        return null;
    }

    /**
     * Remove Cookie
     *
     * Removes cookie if it exists
     *
     * @param string $name
     * @return bool
     */
    public function remove($name) {
        $data   =   [
            'name'  =>  $name,
            'value' =>  null,
            'exp'   =>  time() - 3600,
        ];
        $this->set($data);
        $get = $this->get($name);
        if ( !$get ) {
            return true;
        }
        return false;
    }

} 