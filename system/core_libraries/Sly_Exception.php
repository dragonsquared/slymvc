<?php

namespace SlyLibraries;

/**
 * Class Sly_Exception
 *
 * Exception renderer
 *
 * @package     slyMVC
 * @subpackage  SlyLibraries
 * @author      slyMVC Dev Team
 * @link        http://docs.slymvc.com
 */
class Sly_Exception {

    public function __construct($settings=null) {
        $this->class_settings = $settings;
    }

    public function run($exception) {
        if ( !$this->class_settings['exceptions'] ) {
            $this->render_exception($exception);
        }
    }

    public function render_exception($exception) {
        $trace = $this->get_exception_trace(debug_backtrace(false));
        $data = [
            'exception'                     =>  trim($exception),
            'file'                          =>  'Line #' . $this->get_exception_line($trace) . "\t" .
                $this->get_exception_file($trace),
            'arguments'                     =>  $this->get_exception_args($trace),
            'Namespace\\Class\\Function'    =>  $this->get_exception_class($trace) .
                '\\' . $this->get_exception_function($trace) . '()'
        ];
        include(dirname(__DIR__) . '/system_pages/exception.php');
        exit;
    }

    public function get_exception_trace($trace) {
        return $trace[2];
    }

    public function get_exception_class($trace) {
        return $trace['class'];
    }

    public function get_exception_function($trace) {
        return $trace['function'];
    }

    public function get_exception_line($trace) {
        return $trace['line'];
    }

    public function get_exception_file($trace) {
        return $trace['file'];
    }

    public function get_exception_args($trace) {
        return $trace['args'];
    }

}