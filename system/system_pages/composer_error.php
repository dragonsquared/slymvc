<!DOCTYPE html>
<html>
<head>
    <title><? echo isset($data['installing']) ? "Composer Installing" :  "Composer Error" ?></title>
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,300,700' rel='stylesheet' type='text/css'>
    <style>
        html {
            margin: 0;
            padding: 0;
        }
        body {
            font-family: 'Ubuntu', sans-serif;
            font-weight: 300;
            width: 1200px;
            margin: 0 auto;
        }
        h1, h3 {
            margin: 0;
            padding: 5px 10px;
            color: #fff;
            background-color: #2C3E50;
            font-weight: 300;
        }
        h3 {
            background-color: #999;
            color: #000;
            margin-top: 10px;
            cursor: pointer;
        }
        pre {
            padding: 10px;
            background-color: #CCC;
            margin-top: 0;
            display: none;
        }
        div.info {
            padding: 10px;
            background-color: #CCC;
            margin-top: 0;
            font-size: 14px;
            display: none;
        }
        div.info:first-of-type {
            display: block;
        }
        p {
            padding: 0;
            margin: 0 0 5px 0;
        }
        button {
            margin: 10px 0 0 0;
            cursor: pointer;
        }
        img#loader {
            position: fixed;
            top:15%;
            left:50%;
            margin-left:-110px;
            display:none;
        }
    </style>
</head>
<body>
<img id="loader" src="assets/img/loader.gif" />
<h1><? echo isset($data['installing']) ? "Composer Installing" :  "Composer Error" ?></h1>

<? if ( isset($data['installing']) ): ?>
    <h3>Packages Being Installed</h3>
    <div class="info">
        <p>Please do not refresh the page, it will do it on its own.</p>
        <? for ( $i=1; $i<count($data['packages'])+1;$i++ ): ?>
            <?= $i . '. ' . $data['packages'][$i-1] . '<br />'; ?>
        <? endfor; ?>
        <?=$data['page']?>
    </div>
<?php endif; ?>

<? if ( isset($data['msg']) ): ?>
    <?=$data['msg']?>
<? endif; ?>

<? if ( isset($data['composer_packages_not_installed']) ): ?>

    <div id="install">
        <h3>Composer Packages Not Installed</h3>
        <div class="info">
            Would you like to try and install them from here?<br />
            <button id="install_packages">Install</button>
        </div>
    </div>

    <div id="pkgs" style="display:none;">
        <h3>Packages Being Installed</h3>
        <div class="info">
            <p>Please do not refresh the page, it will do it on its own.</p>
            <? for ( $i=1; $i<count($data['packages'])+1;$i++ ): ?>
                <?= $i . '. ' . $data['packages'][$i-1] . '<br />'; ?>
            <? endfor; ?>
        </div>
    </div>

<? endif; ?>

<? if ( isset($data['composer_not_installed']) ): ?>

    <h3>Composer could not be found</h3>
    <div class="info">Here are some options</div>

    <h3>Search for Composer</h3>
    <div class="info" style="display:block;">
        <input type="checkbox" id="require_phar" /> - Require .phar?<br />
        <input type="checkbox" id="require_php" /> - Require php prepend?<br />
        <input type="text" id="composer_full" placeholder="Composer Full Path" />
        <button id="full_search">Search</button> - Composer Full Path
        <br />
        <input type="text" id="composer_name" placeholder="Composer Name" />
        <button id="name_search">Search</button> - Does it go by a different name?
    </div>

    <h3>Install packages without Composer</h3>
    <div class="info">
        <p>We can attempt to install the packages needed for slyMVC.</p>
    </div>

<? endif; ?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('h3').click(function(){
            var pre_vis = $('div.info:visible');
            if ( pre_vis.length > 1 ) {
                pre_vis.not($(this).next()).slideUp();
            }
            if ( !$(this).next('pre').is(':visible') ) {
                pre_vis.slideUp();
                $(this).next().slideDown();
            }
        });
        $('#install_packages').click(function(){
            $('#install').hide();
            $('#pkgs').show();
            $('#loader').show();
            var url = window.location.href;
            var ins = null;
            if ( url.indexOf('?') != -1 ) {
                ins = '&install=true';
            } else {
                ins = '?composer_name=composer&install=true';
            }
            var g_url = url + ins;
            $.get(g_url, function(data) {
                    window.location.href = url
                }
            );
        });
        $('#name_search').click(function(){
            var cn = $('#composer_name');
            if ( cn.val() == '' ) {
                alert('Must supply name to try');
                cn.focus();
            } else {
                var vt = '?composer_name=' + cn.val();
                window.location.href = document.URL.split('?')[0] + construct_url(vt);
            }
        });
        $('#full_search').click(function(){
            var cf = $('#composer_full');
            if ( cf.val() == '' ) {
                alert('Full path must be provided');
                cf.focus();
            } else {
                var vt = '?composer_path=' + cf.val();
                window.location.href = document.URL.split('?')[0] + construct_url(vt);
            }

        });
    });
    function construct_url(val) {
        var url = val;
        if ( $('#require_phar').prop('checked') == true ) {
            url += '&phar=true';
        }
        if ( $('#require_php').prop('checked') == true ) {
            url += '&php=true';
        }
        return encodeURI(url);
    }
</script>
</body>
</html>