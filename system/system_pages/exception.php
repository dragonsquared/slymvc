<!DOCTYPE html>
<html>
<head>
    <title>Exception</title>
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,300,700' rel='stylesheet' type='text/css'>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <style>
        html {
            margin: 0;
            padding: 0;
        }
        body {
            font-family: 'Ubuntu', sans-serif;
            font-weight: 300;
            width: 1200px;
            margin: 0 auto;
        }
        h1, h3 {
            margin: 0;
            padding: 5px 10px;
            color: #fff;
            background-color: #2C3E50;
            font-weight: 300;
        }
        h3 {
            background-color: #999;
            color: #000;
            margin-top: 10px;
            cursor: pointer;
        }
        pre {
            padding: 10px;
            background-color: #CCC;
            margin-top: 0;
            display: none;
        }
        pre:first-of-type {
            display: block;
        }
        button {
            margin: 10px 0 0 0;
        }
    </style>
</head>
<body>
<h1>Exception</h1>
<button id="collapse-all">Collapse All</button>
<button id="expand-all">Expand All</button>
<? foreach ( $data as $k => $v ): ?>
    <h3><? echo ucwords($k); ?></h3>
    <pre><? print_r($v); ?></pre>
<? endforeach; ?>
<script>
    $(document).ready(function(){
        $('h3').click(function(){
            var pre_vis = $('pre:visible');
            if ( pre_vis.length > 1 ) {
                pre_vis.not($(this).next()).slideUp();
            }
            if ( !$(this).next('pre').is(':visible') ) {
                pre_vis.slideUp()
                $(this).next().slideDown();
            }
        });
        $('#collapse-all').click(function(){
            $('pre').slideUp();
        });
        $('#expand-all').click(function(){
            $('pre').slideDown();
        });
    });
</script>
</body>
</html>