<style>
    section {
		max-width: 1200px;
        margin: 0 auto 10px;
        font-family: 'Courier New', sans-serif;
        display:block;
        box-shadow: 0 0 8px #CCC;
        font-size: 12px;
    }
    section span.title {
        display:block;
        padding: 5px;
        cursor: pointer;
        background: #2C3E50;
        color: #fff;
    }
    section span.title:hover {
        background: #3498DB;
    }
    section span.info {
        display: block;
        padding: .5em;
    }
    section span.key {
        display:inline-block;
        width: 30%;
    }
    section span.value {
        display:inline-block;
        width: 65%;
    }
    div.information {
        display: none;
    }
</style>
<? foreach ( $data['all'] as $k => $v ): ?>
    <section>
        <span class="title"><?=ucwords(str_replace('_', ' ', $k))?></span>
        <div class="information">
            <? foreach ( $v as $k1 => $v1 ): ?>
                <? if ( !is_object($v1) ): ?>
                    <span class="info">
                        <span class="key"><?=$k1?></span>
                        <span class="value"><?=$v1?></span>
                    </span>
                <? else: ?>
                    <? $v1 = (array) $v1 ?>
                    <? $i = 0; ?>
                    <? foreach ( $v1 as $item ): ?>
                        <? if ( $i == 0 ) : ?>
                            <span class="info">
                            <span class="key"><?=$k1?></span>
                            <span class="value"><?=$item?></span>
                        </span>
                        <? else: ?>
                            <span class="info">
                            <span class="key"></span>
                            <span class="value"><?=$item?></span>
                        </span>
                        <?endif; ?>
                        <?$i++?>
                    <? endforeach; ?>
                <? endif; ?>
            <? endforeach; ?>
        </div>
    </section>
<? endforeach; ?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
    $(function(){
        $('section:first').find('div.information').show();
        $('span.title').click(function(){
            $('div.information').slideUp();
            $(this).parent().find('div.information').slideDown();
        });
    });
</script>