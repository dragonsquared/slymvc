<!DOCTYPE html>
<html>
<head>
    <title><? echo isset($data['title']) ? $data['title'] : null ?></title>
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,300,700' rel='stylesheet' type='text/css'>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <style>
        html {
            margin: 0;
            padding: 0;
        }
        body {
            font-family: 'Ubuntu', sans-serif;
            font-weight: 300;
            width: 1200px;
            margin: 0 auto;
        }
        h1, h3 {
            margin: 0;
            padding: 5px 10px;
            color: #fff;
            background-color: #2C3E50;
            font-weight: 300;
        }
        h3 {
            background-color: #999;
            color: #000;
            margin-top: 10px;
        }
        pre {
            padding: 10px;
            background-color: #CCC;
            margin-top: 0;
        }
        button {
            margin: 10px 0 0 0;
        }
    </style>
</head>
<body>
<h3><? echo $data['title']; ?></h3>
<pre><? echo isset($data['msg']) ? $data['msg'] : null; ?></pre>
</body>
</html>