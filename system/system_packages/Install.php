<?php

namespace SysPack;

require_once SYS_FOLDER . 'core_libraries/Sly_Exception.php';
require_once SYS_FOLDER . 'core_libraries/Sly_Input.php';
require_once SYS_FOLDER . 'core_libraries/Sly_Cache.php';

use SlyLibraries\Sly_Exception;
use SlyLibraries\Sly_Input;
use SlyLibraries\Sly_Cache;

class Install {

    protected $except;
    protected $input;
    protected $cache;
    protected $install = null;
    protected $path = null;

    public function __construct() {

        error_reporting(E_ALL);

        $this->except = new Sly_Exception();
        $this->input = new Sly_Input();
        $settings = ['cache_folder'=>APP_FOLDER.'cache'];
        $this->cache = new Sly_Cache($settings);

        $this->test = exec('composer --version');

    }

    public function update_test() {

        $phar   = $this->input->get('phar') ? '.' . $this->input->get('phar') : null;
        $php    = $this->input->get('php');

        if ( $composer_name = $this->input->get('composer_name') ) {
            $this->path = $php . ' ' . $composer_name . $phar;
            $this->test = exec($this->path);
        }

        if ( $composer_path = $this->input->get('composer_path') ) {
            $this->path = $composer_path;
            $this->test = exec($composer_path);
        }

        if ( $this->input->get('install') ) {
            $this->install = 1;
        }

    }

    public function test_for_composer() {

        $data = [
            'baseUrl' => $this->base_url()
        ];

        if ( empty($this->test) ) {
            $data['composer_not_installed'] = 1;
            $this->except->composer_error($data);
        } else {
            if ( $this->install ) {
                $this->composer_install();
            }
            $data['composer_packages_not_installed'] = 1;
            $packages = file_get_contents(SYS_FOLDER . 'composer.json');
            $packages = json_decode($packages, true);
            $pks = [];
            foreach ( $packages['require'] as $k => $v ) {
                $px = explode('/', $k);
                $pks[] = $px[0] == $px[1] ? $px[0] : $px[0] . ' - ' . $px[1];
            }
            $data['packages'] = $pks;
            $this->except->composer_error($data);
        }

        exit;

    }

    public function composer_install() {
        $data = [];
        putenv('COMPOSER_HOME='.$this->path);
        $cmd = 'cd system && ' . $this->path . ' install 2>&1';
        exec($cmd, $data, $ret);
        if ( $ret ) {
            echo json_encode(['success'=>true]);
        }
    }

    public function set_perms() {
        $folders = [
            APP_FOLDER . 'cache',
            APP_FOLDER . 'system_cache',
            SYS_FOLDER . 'system_cache'
        ];
        $msg = '';
        foreach ( $folders as $folder ) {
            $msg .= $this->test_perms($folder);
        }
        if ( !empty($msg) ) {
            $msg .= 'This may be a false exception, please try refreshing the page.';
            $this->except->except_error($msg);
            exit;
        }
    }

    public function test_perms($folder) {
        $test = $this->check_permissions($folder);
        if ( $test != 777 ) {
            $this->update_perms($folder);
            if ( $this->check_permissions($folder) != 777 ) {
                return 'Could not update permissions for : ' . $folder . '<br />';
            }
        }
    }

    public function update_perms($folder) {
        exec('chmod 0777 ' . $folder);
        return (int) $this->check_permissions($folder);
    }

    public function check_permissions($file) {
        return (int) substr(sprintf('%o', fileperms($file)), -4);
    }

    public function base_url() {
        $s          =   empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
        $sp         =   strtolower($_SERVER["SERVER_PROTOCOL"]);
        $protocol   =   substr($sp, 0, strpos($sp, "/")) . $s;
        $port       =   ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
        $full       =   $protocol . '://' . $_SERVER['SERVER_NAME'] . $port;
        $ext        =   $_SERVER['REQUEST_URI'];
        if ( strpos($ext, '?') !== false ) {
            $ex = explode('?', $ext);
            $ext = $ex[0];
        }
        return $full . $ext;
    }

} 