<?php

namespace SlyCore;


class Sly_Loader {

    public function __construct() {

    }

    public function model($filename) {
        return $this->loader($filename, 'Models');
    }

    public function helper($filename) {
        return $this->loader($filename, 'Helpers');
    }

    public function controller($filename) {
        return $this->loader($filename, 'Controllers');
    }

    public function library($filename) {
        return $this->loader($filename, 'Libraries');
    }

    public function models($filename) {
        return $this->loader($filename, 'Models');
    }

    public function helpers($filename) {
        return $this->loader($filename, 'Helpers');
    }

    public function controllers($filename) {
        return $this->loader($filename, 'Controllers');
    }

    public function libraries($filename) {
        return $this->loader($filename, 'Libraries');
    }

    public function core($filename) {
        return $this->loader($filename, 'Core');
    }

    protected function loader($filename, $type) {
        $types = [
            'Models'        =>  'models',
            'Helpers'       =>  'helpers',
            'Controllers'   =>  'controllers',
            'Libraries'     =>  'libraries',
            'Core'          =>  'core'
        ];
        $class = "\\$type\\" . str_replace('/', '\\', $filename);
        $filename = strpos($filename, '.php') === false ? $filename . '.php' : $filename;
        $file = APP_FOLDER . $types[$type] . '/' . $filename;
        if ( !is_file($file) ) {
            echo '<pre>';
            echo 'Cannot find : ' . $file;
            exit;
        }
        require_once $file;
        return new $class();
    }

} 