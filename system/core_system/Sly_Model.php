<?php

namespace SlyCore;

require_once APP_FOLDER . 'config/database.php';

use Config\DatabaseConfig;
use Doctrine\DBAL;

class Sly_Model extends Sly_Core {

    public function __construct() {
        parent::__construct();
    }

    protected function db() {
        $db             =   new DatabaseConfig();
        $db_settings    =   $db->db_config();

        $driver	=  $db_settings['driver'];
        $host	=  $db_settings['host'];
        $user	=  $db_settings['username'];
        $pass	=  $db_settings['password'];
        $name	=  $db_settings['name'];
        $port	=  isset($db_settings['port']) ? $db_settings['port'] : null;
        $enc	=  isset($db_settings['encoding']) ? $db_settings['encoding'] : null;

		$config = new DBAL\Configuration();
		
		$configParams 	= [
			'dbname' 	=>	$name,
			'user' 		=>	$user,
			'password' 	=>	$pass,
			'host' 		=>	$host,
			'driver' 	=>	$driver
		];
		
        $conn = DBAL\DriverManager::getConnection($configParams, $config);

        return $conn;

    }

} 