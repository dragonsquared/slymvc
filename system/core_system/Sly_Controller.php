<?php

namespace SlyCore;

require_once APP_FOLDER . 'config/autoload.php';

use SlyLibraries\Sly_Base;
use SlyLibraries\Sly_Cache;
use SlyLibraries\Sly_Cookies;
use SlyLibraries\Sly_Encrypt;
use SlyLibraries\Sly_Files;
use SlyLibraries\Sly_Images;
use SlyLibraries\Sly_Init;
use SlyLibraries\Sly_Input;
use SlyLibraries\Sly_Mailer;
use SlyLibraries\Sly_Session;
use SlyLibraries\Sly_Upload;
use Config\Autoloader;

/**
 * Class Sly_Controller
 *
 * Core Controller
 *
 * @package     slyMVC
 * @subpackage  SlyCore
 * @author      slyMVC Dev Team
 * @link        http://docs.slymvc.com
 */
class Sly_Controller extends Sly_Core {

    /**
     * Constructor
     *
     * Instantiate App Model and Core Libraries, as well as set up session defaults
     */
    public function __construct() {
        parent::__construct();

        $this->load->core('AppModel');

        // Instantiate instances of Core Libraries
        $this->encrypt  = new Sly_Encrypt();
        $this->cookie   = new Sly_Cookies();
        $this->input    = new Sly_Input();
        $this->image    = new Sly_Images();
        $this->upload   = new Sly_Upload($this->settings['upload']);
        $this->cache    = new Sly_Cache($this->settings);
        $this->files    = new Sly_Files();
        $this->base     = new Sly_Base();
        $this->ini      = new Sly_Init();
        $this->mailer   = new Sly_Mailer();

        // Ensure that Cache Folder has proper permissions
        $this->cache->test_cache_folder();

        // Instantiate instance of Session Library and set user stats into session
        $this->session = new Sly_Session();

        $this->session->get('ip_address') || $this->session->set('ip_address', $this->ip_address());
        $this->session->get('user_agent') || $this->session->set('users_agent', $this->agent());

        $autoload = new Autoloader();

        $this->load_array = $autoload->load_array();

        $this->sly_autoload();

        $class_load_time = microtime();

        defined('class_load_time') or define('class_load_time', round($class_load_time - start_load, 3));
    }

    /**
     * Sly autoload
     *
     * Autoloads all core classes, as well as the user defined classes in application/config/autoload.php
     */
    public function sly_autoload() {

        $callable = [
            'models'    =>  'model',
            'helpers'   =>  'helper',
            'libraries' =>  'library'
        ];

        $load_array = $this->load_array;

        foreach ( $load_array as $key => $val ) {
            $load_type = $key;
            if ( count($val) > 0 ) {
                foreach ( $val as $k => $v ) {
                    $class_call_name = null;
                    if ( !is_numeric($k) ) {
                        $class_call_name = $v;
                        $item = $k;
                    } else {
                        $item = $v;
                    }
                    $class = $item;
                    if ( strpos($class, $load_type) === false ) {
                        $class = $class . '_' . $callable[$load_type];
                    }
                    $class = strtolower($class);
                    if ( $class_call_name ) {
                        if ( !isset($this->$class_call_name) ) {
                            $this->$class_call_name = $this->load->$load_type($item);
                        }
                    } else {
                        if ( !isset($this->$class) ) {
                            $this->$class = $this->load->$load_type($item);
                        }
                    }

                }
            }
        }

    }

    /**
     * Redirect
     *
     * Redirects to the provided url, and with the second parameter used can also pass an http code
     *
     * @param string null $url
     * @param string null $code
     */
    public function redirect($url=null, $code=null) {
        $url    =   $url ? $url : $this->base_url() . $url;
        if ( strpos('http', $url) !== false ) {
            $url    =   $this->base_url() . $url;
        }
        if ($code) {
            header('Location: ' . $url, TRUE, $code);
            exit;
        }
        header('Location: ' . $url);
        exit;
    }

    /**
     * JSON
     *
     * Converts an array to JSON
     *
     * @param $array
     * @return string
     */
    public function json($array) {
        return json_encode($array);
    }

    /**
     * Obj
     *
     * Recursively creates a standard class object from the passed in array
     *
     * @param $array
     * @return \stdClass
     */
    public function obj($array) {
        $obj = new \stdClass;
        foreach($array as $k => $v) {
            if(strlen($k)) {
                if(is_array($v)) {
                    $obj->{$k} = $this->obj($v);
                } else {
                    $obj->{$k} = $v;
                }
            }
        }
        return $obj;
    }

    /**
     * Printer
     *
     * Cleanly formatted print_r method that takes an array and displays it on
     * screen as either an array, JSON, or an object
     *
     * @param $array
     * @param null $method defaults to array in application/config/config.php
     */
    public function printer($array, $method=null) {
		$method = $method ? $method : $this->settings['printer_output'];
        echo '<pre>';
        print_r($method ? $this->ret_data($array, $method) : $array);
        echo '</pre>';
    }

    /**
     * Dumper
     *
     * Cleanly formatted var_dump method that takes a dump with an array all over the screen
     *
     * @param $array
     */
    public function dumper($array) {
        echo '<pre>';
        var_dump($array);
        echo '</pre>';
    }

    /**
     * Get Namespaces
     *
     * Returns an array of all Declared Namespaces
     *
     * @return array
     */
    public function get_namespaces() {
        $namespaces = [];
        foreach ( get_declared_classes() as $name ) {
            if(preg_match_all("@[^\\\]+(?=\\\)@iU", $name, $matches)) {
                $matches = $matches[0];
                $parent =&$namespaces;
                while(count($matches)) {
                    $match = array_shift($matches);
                    if(!isset($parent[$match]) && count($matches))
                        $parent[$match] = array();
                    $parent =&$parent[$match];

                }
            }
        }
        return $namespaces;
    }

    /**
     * Get Classes
     *
     * Returns an array of all Declared Classes
     *
     * @return array
     */
    public function get_classes() {
        $classes = [];
        $user_classes = [
            'SlyCore',
            'SlyLibraries',
            'Controllers',
            'Models',
            'Helpers',
            'Libraries',
            'Core'
        ];
        foreach ( get_declared_classes() as $class ) {
            foreach ( $user_classes as $user ) {
                if ( strpos($class, $user) !== false ) {
                    if ( !in_array($class, $classes) ) {
                        $cX = explode('\\', $class);
                        if ( $cX[0] == $user ) {
                            $classes[$cX[0]][] = $cX[1];
                        }
                    }
                }
            }
        }
        asort($classes);
        $sorted = $classes;
        $classes = [];
        foreach ( $sorted as $key => $class ) {
            $classes[$key] = $class;
        }
        return $classes;
    }

    /**
     * Return Data
     *
     * Returns all data in the preferred format, defaults as an array
     *
     * @param $data
     * @param null $method
     * @return array|mixed|\stdClass|string
     */
    public function ret_data($data, $method=null) {

        // Normalize the data to a standard array and return desired type

        if ( $method ) {

            if ( !is_array($data) ) {
                if (is_object($data)) {
                    $data = (array)$data;
                } else {
                    if ( $this->isJson($data) ) {
                        $data = json_decode($data, true);
                    }
                }
            }

            switch ($method) {
                case 'obj':
                    $data = $this->obj($data);
                    break;
                case 'object':
                    $data = $this->obj($data);
                    break;
                case 'json':
                    $data = $this->json($data);
                    break;
            }
            return $data;
        }

        return $data;

    }

    /**
     * is JSON
     *
     * Verifies passed in data is JSON with a return of true/false
     *
     * @param $data
     * @return bool
     */
    public function isJson($data) {
		if(is_string($data)){
        	json_decode($data);
        	return (bool) json_last_error() == JSON_ERROR_NONE;
		}
		
		return false;
    }

    /**
     * Format Microtime
     *
     * Returns passed in microtime as a date formatted string
     *
     * @param $microtime
     * @return string
     */
    public function format_microtime($microtime) {
        list($usec, $sec) = explode(' ', $microtime);
        $usec = ltrim($usec, '0');
        return date('H:i:s', $microtime) . ltrim(round($usec, 3), '0');
    }

}