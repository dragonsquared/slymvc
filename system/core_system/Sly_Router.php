<?php

namespace SlyCore;

use Slim\Environment;
use Slim\Http\Request;
use Slim\Middleware\PrettyExceptions;
use Slim\Route;

Class Sly_Router {

    protected $routes;
    protected $request;
    protected $errorHandler;

    public function __construct($settings) {
        $env            =   Environment::getInstance();
        $this->request  =   new Request($env);
        $this->routes   =   [];
        $this->excep    =   new PrettyExceptions();
    }

    /**
     * @param array $routes
     */

    public function addRoutes($routes) {

        foreach ( $routes as $route => $path ) {

            $method =   "any";

            if (strpos($path, "@") !== false) {
                list($path, $method) = explode("@", $path);
            }

            $func   =   $this->processCallback($path);

            $r      =   new Route($route, $func);
            $r->setHttpMethods(strtoupper($method));

            array_push($this->routes, $r);

        }

    }

    /**
     * @param $path
     * @return callable
     */

    protected function processCallback($path) {

        $class  =   "Main";

        if (strpos($path,":") !== false) {
            list($class, $path) = explode(":", $path);
        }

        $function   =   ($path != "") ? $path : "index";

        $func   =   function() use ($class, $function) {
            $class  =   '\controllers\\' . $class;
            if ( !class_exists($class) ) {
                $file   =   APP_FOLDER . str_replace('\\', '/', $class) . '.php';
                if ( file_exists($file)) {
                    require $file;
                }
            }
            $class  =   new $class();
            $args   =   func_get_args();
            return call_user_func_array([$class, $function], $args);
        };
        return $func;
    }

    /**
     * Runs the view creator
     */

    public function run() {

        $display404 = true;
        $uri        = $this->request->getResourceUri();
        $method     = $this->request->getMethod();

        foreach ( $this->routes as $i => $route ) {
            if ( $route->matches($uri) ) {
                if ( $route->supportsHttpMethod($method) || $route->supportsHttpMethod("ANY") ) {
                    call_user_func_array($route->getCallable(), array_values($route->getParams()));
                    $display404 = false;
                }
            }
        }

        if ($display404) {
            if (is_callable($this->errorHandler)) {
                call_user_func($this->errorHandler);
            } else {
                echo "404 - route not found";
            }
        }

    }

    /**
     * Sets 404 view
     *
     * @param string $path
     */

    public function set404handler($path) {
        $this->errorHandler = $this->processCallback($path);
    }

}