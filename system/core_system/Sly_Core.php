<?php

namespace SlyCore;

require_once APP_FOLDER . 'config/config.php';
require_once APP_FOLDER . 'config/routes.php';

use Config\Config;
use Config\Routes;
use \Slim\Slim;

class Sly_Core extends Slim {

    public $settings;
    public $routes;
    public $data        =   [];

    public function __construct() {

        $cfg = new Config();
        $routes = new Routes();

        $settings = $cfg->cfg();

        if ( !$settings ) {
            print "Your application has no settings. Please check : " . APP_FOLDER . "config/config.php";
            exit;
        }

        parent::__construct($settings);

        $this->load = new Sly_Loader();

        $this->routes = $routes->get_routes();

        $this->router = new Sly_Router($settings);

        $this->settings = $settings;

    }

    public function base_url() {
        $s          =   empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
        $sp         =   strtolower($_SERVER["SERVER_PROTOCOL"]);
        $protocol   =   substr($sp, 0, strpos($sp, "/")) . $s;
        $port       =   ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
        $full       =   $protocol . '://' . $_SERVER['SERVER_NAME'] . $port . '/';
        if ( $this->settings['site_url'] !== '' ) {
            $full = $full . $this->settings['site_url'] . '/';
        }
        return $full;
    }

    public function full_url() {
        return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    public function title($title, $uc_words=null) {
        if ( is_array($title) ) {
            $title = implode(" " . $this->settings['site_title_sep'] . " ", $title);
        }
        $this->data['page_title']    =   $uc_words ? ucwords($title) : $title;
    }

    public function view($view=null) {
        if ( strpos($view, '.php') === false ) {
            if ( strpos($view, '.twig') === false ) {
                $view = $view . '.twig';
            }
        }
        $this->data['view']  =   $view;
    }

    public function render($template=null, $data=null, $status=null) {
        if ( $template ) {
            if ( is_array($template) ) {
                $data       =   $template;
                $template   =   $this->settings['default_template'];
            } else {
                $data       =   $this->data;
            }
        } else {
            $data = $this->data;
            $template = $this->settings['default_template'];
        }

        if(strpos($template, ".php") === false ) {
            if (strpos($template, ".twig") === false) {
                $template = $template . ".twig";
            }
        }

        if(strpos($template, "templates/") === false) {
            $template   =   'templates/' . $template;
        }

        $data['app_folder'] = APP_FOLDER;
        $data['base_url']   = $this->base_url();
        $data['site_title'] = $this->settings['site_title_sep'] . ' ' . $this->settings['site_title'];

        Slim::render($template, $data, $status);
    }

    public function run() {

        $this->router->addRoutes($this->routes);

        $this->router->set404handler($this->settings['404']);

        $this->router->run();

    }

    public function ip_address() {
        return $_SERVER['REMOTE_ADDR'];
    }

    public function agent() {
        return $_SERVER['HTTP_USER_AGENT'];
    }

}