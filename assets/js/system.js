$(function(){
    if ( window.Profiler ) {
        $.get(
            window.baseUrl + 'raw/profiler?data=' + window.ProfilerData,
            function(data) {
                $(data).appendTo('body');
            }
        )
    }
});