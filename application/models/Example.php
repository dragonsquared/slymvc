<?php

namespace Models;

use Core\AppModel;

class Example extends AppModel {

    public function __construct() {
        parent::__construct();
    }
    
	public function get_users(){
		return $this->db->query("SELECT * FROM examples")->fetchAll();
	}
	
	public function get_users_ar(){
		return $this->builder->select('*')->from('examples')->execute()->fetchAll();
	}
}