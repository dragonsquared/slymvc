<?php

namespace Config;

class Routes {

    /**
     * Routes are an array of key/values
     *
     * The key is the web address you want to route to
     *
     * The value is constructed in the following method
     *
     * controller:function@ get|post
     *
     * @return array
     */

    public function get_routes() {

        /**
         * There are no reserved routes, do what you want with them.
         * The only require route is / which points to your home page.
         */

        $routes = [

            // Required
            '/'             => 'Welcome:index@get',

            // Image Manipulation
            '/image/resize' => 'System:resize_image@get',
            '/image/thumb'  => 'System:thumb_image@get',
            '/image/get'    => 'System:get_image@get',

            // Mailer Testing
            '/mailer/template(/:name)' => 'Mailer:template@get',

            // Debugging
            '/raw(/)'       =>  'Raw:index@get',
            '/raw/profiler' =>  'Raw:profiler@get'

        ];

        return $routes;

    }

}