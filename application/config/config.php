<?php

namespace Config;

class Config {

    public function cfg() {


        /**
         * Views configuration
         *
         * This should not need to be changed unless you want to alter your default template
         *
         */

        $config['view']                 =   new \Slim\Views\Twig();
        $config['templates.path']       =   dirname(__DIR__) . '/views';
        $config['default_template']     =   'index.twig';

        // 404 Page
        $config['404']                  =   'System:error';                     // Does not require get|post operator

        // Should not need to changed
        $config['views_directory']      =   APP_FOLDER . 'views';
        $config['cache_folder']         =   APP_FOLDER . 'cache';

        // Optional App Configurations
        $config['exceptions']           =   true;
        $config['cache_default_ret']    =   'array';                            // array | json | obj
        $config['printer_output']    	=   'array';                            // array | json | obj

        // Site Page Config
        $config['site_title']           =   'slyMVC';                           // Base title of application
        $config['site_title_sep']       =   '|';                                // Separator for your title
        $config['site_url']             =   '';                           		// Leave blank if not a sub-directory

        // Encryption Settings
        $config['encrypt_key']          =   "5971563548152618";                 // 8 | 16 | 32 random string of numbers

        // Mailer settings
        $config['mail_type']            =   'mail';                             // mail or smtp
        $config['mail_host']            =   '';                                 // SMTP mail settings only
        $config['mail_port']            =   '';                                 // SMTP mail settings only
        $config['mail_username']        =   '';                                 // SMTP mail settings only
        $config['mail_password']        =   '';                                 // SMTP mail settings only

        // Upload Settings
        $config['upload']['max_size']      =  2048;                             // Size in kilobytes KB     - Set 0 for no max
        $config['upload']['max_height']    =  1080;                             // Size in pixels           - Set 0 for no max
        $config['upload']['max_width']     =  1900;                             // Size in pixels           - Set 0 for no max
        $config['upload']['upload_path']   =  APP_FOLDER . 'uploads';           // Path must be writable
        $config['upload']['overwrite']     =  False;                            // Overwrite existing files
        $config['upload']['file_name']     =  null;                             // Write the file using this name
        $config['upload']['allowed_types'] =  ['jpg', 'jpeg', 'gif', 'png'];    // Allowed file types
        $config['upload']['encrypt_name']  =  False;                            // Encrypt the file name
        $config['upload']['remove_spaces'] =  True;                             // Replace spaces with an underscore

        return $config;
    }

}
