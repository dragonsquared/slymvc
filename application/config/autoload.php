<?php

namespace Config;

class Autoloader {

	/**
     * Will automatically append the singular version of the array keys to autoload classes
     *
     * Example :
     *
     * protected $models = [
     *      'Users'
     * ];
     *
     * Can be accessed as $this->users_model when loaded
     *
     * Using the class as a key with a value will autoload the class with the value as the callable
     * class
     *
     * Example :
     *
     * protected $libraries = [
     *      'Test' => 'test'
     * ];
     *
     * Will result in being able to call the class as $this->test instead of $this->test_library
     *
     */

    protected $models       = [];

    protected $libraries    = [];

    protected $helpers      = [];

    public function load_array() {

        $autoload   =   [
            'libraries' =>  $this->libraries,
            'models'    =>  $this->models,
            'helpers'   =>  $this->helpers
        ];

        return $autoload;

    }

}