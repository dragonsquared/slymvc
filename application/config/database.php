<?php

namespace Config;

/**
 * Class DatabaseConfig
 * @package Config
 */

class DatabaseConfig {

    /*
	| -------------------------------------------------------------------
	| DATABASE CONNECTIVITY SETTINGS
	| -------------------------------------------------------------------
	| This section will contain the settings needed to access your database.
	|
	| -------------------------------------------------------------------
	| EXPLANATION OF VARIABLES
	| -------------------------------------------------------------------
	|
	|	'driver'    The database type. ie: pdo_mysql.
	|               Currently supported:
	|				    pdo_mysql       MySQL
	|                   pdo_oracle      Oracle
	|                   pdo_pgsql       Postgres SQL
	|                   pdo_sqlsrv      Microsoft SQL Server
	|                   pdo_sqlite      SQL Lite
	|                   pdo_ibm         IBM
	|
	*/

    public function db_config() {

		$database  =   [];
		
		$database['host']		= 'localhost';
		$database['username']	= '';
		$database['password']	= '';
		$database['name']		= '';
		$database['driver']		= 'pdo_mysql';
		
		/*
		* Not required fields
		* $database['port']		= 3306;
		* $database['encoding']	= 'UTF8';
		*/
		
        return $database;
    }

}