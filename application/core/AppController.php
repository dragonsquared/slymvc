<?php

namespace Core;

use SlyCore\Sly_Controller;

class AppController extends Sly_Controller {

    public function __construct() {
        parent::__construct();

        $this->base_url             = $this->base_url();
        $this->assets               = $this->base_url() . 'assets/';
        $this->styles               = $this->base_url() . 'assets/styles/';
        $this->js                   = $this->base_url() . 'assets/js/';
        $this->img                  = $this->base_url() . 'assets/img/';

        $this->data['base_url']     = $this->base_url;
        $this->data['asset_url']    = $this->assets;
        $this->data['styles']       = $this->styles;
        $this->data['js']           = $this->js;
        $this->data['img']          = $this->img;
        $this->data['site_title']   = $this->settings['site_title'];

        $this->data['profiler']     = 'false';

        $all_data                   = $this->all_data(null, 'obj');

        $this->data_obj             = $all_data;
        $this->data_arr             = (array) $all_data;
        $this->data_json            = json_encode($all_data);

        $this->data['all_json']     = $this->data_json;

    }

    /**
     * for debugging purposes
     *
     * prints all $this->data and session data
     *
     * @param string $method
     * @param bool $printer
     * @return mixed array|string
     */

    public function all_data($printer=null, $method=null) {
        $start_time = microtime();
        $data = [
            'data'      => $this->data,                         // Data being passed to all views
            'session'   => $this->session->all(),               // All Session Data
            'classes'   => $this->get_classes(),                // Classes being called by the script
            'variables' => get_defined_vars(),                  // Defined variables
            'constants' => get_defined_constants(true)['user'], // Defined Constants (user only)
            #'browser'   => $this->browser()                     // Browser Capabilities
        ];
        $end_time = microtime();
        $load_time['load_time'] = [
            'base_class_load_time' => class_load_time . ' seconds',
            'controller_execution_time' => round($end_time - $start_time, 3) . ' seconds',
            'total_load_time' => round($end_time - start_load, 3) . ' seconds',
        ];
        $data = array_merge($load_time, $data);
        if ( $printer ) {
            $this->printer($data, $method);
        } else {
            return $this->ret_data($data, $method);
        }
    }

} 