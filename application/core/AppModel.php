<?php

namespace Core;

use SlyCore\Sly_Model;

class AppModel extends Sly_Model {

    public function __construct() {
        parent::__construct();

        $this->dt       = date('Y-m-d');
        $this->dtime    = date('Y-m-d H:i:s');

        $this->db       = $this->db();
		
		$this->builder 	= $this->db->createQueryBuilder();
		
    }

} 