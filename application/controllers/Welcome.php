<?php

namespace Controllers;

use Core\AppController;

class Welcome extends AppController{

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        /**
         * You can pass data to the views using the $this->data[var] method
         *
         * $this->title will accept a string or an array and use the site title separator to create the page title
         *
         * $this->view is the view you wish to render into the template
         *
         * $this->render accepts 3 parameters
         *      template -> defaults to the default_template in the configuration file
         *      data -> data to be passed to the view : defaults to application available $this->data
         *      status -> page status : defaults to null -> use standard html status codes
         *
         */

        $this->data['msg'] = 'Something to read';
        $this->data['load_time'] = (array) $this->data_obj->load_time;
        $this->data['start_time'] = microtime();

        $this->title('Welcome');
        $this->view('site/index');
        $this->render('system');
    }
    
}