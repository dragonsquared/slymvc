<?php

namespace Controllers;

use Core\AppController;

class Raw extends AppController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->printer($this->data_obj);
    }

    public function profiler() {
        if ( $this->input->get('data') ) {
            $data['all'] = $this->obj(json_decode($this->input->get('data'), true));
        } else {
            $data['all'] = (array)$this->data_obj;
        }
        include BASEPATH . '/system/system_pages/profiler.php';
    }

} 