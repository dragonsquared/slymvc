<?php

namespace Controllers;

use SlyCore\Sly_Controller;

class System extends Sly_Controller {

    public function __construct() {
        parent::__construct();
        $this->img_path = BASEPATH . '/assets/img/';
    }

    public function error() {
        $this->title('404');
        $this->view('system/404');
        $this->render('system');
    }

    public function get_image() {
        $_GET || $this->redirect($this->base_url());
        $file = $this->input->get('file');
        $file || $this->redirect($this->base_url());
        $filename   =   $file;
        $ext        =   explode(".", $filename);
        $ext        =   $ext[count($ext)-1];
        $filename   =   $this->img_path . $filename;
        if ( !file_exists($filename) ) {
            $this->redirect($this->base_url() . '404');
        }
        header("Content-type: image/$ext;");
        print file_get_contents($filename);
    }

    public function resize_image() {
        $_GET || $this->redirect($this->base_url());
        $file   =   $this->input->get('file');
        $file || $this->redirect($this->base_url());
        $file   =   $this->img_path . $file;
        if ( !file_exists($file) ) {
            $this->redirect($this->base_url() . '404');
        }
        $size   =   $this->input->get('size');
        $size   =   $size ? $size : 50;
        $save   =   $this->input->get('save');
        echo $this->image->resize($file, $size, $save)->show();
    }

    public function thumb_image() {
        $_GET || $this->redirect($this->base_url());
        $file   =   $this->input->get('file');
        $file || $this->redirect($this->base_url());
        $file   =   $this->img_path . $file;
        if ( !file_exists($file) ) {
            $this->redirect($this->base_url() . '404');
        }
        $size   =   $this->input->get('size');
        $size   =   $size ? $size : 100;
        $save   =   $this->input->get('save');
        echo $this->image->thumb($file, $size, $save)->show();
    }

} 