<?php

namespace Controllers;

use Core\AppController;

require_once APP_FOLDER . 'mail_templates/Templates.php';

class Mailer extends AppController {

    protected $templates;

    public function __construct() {
        parent::__construct();
        $this->templates = new \Templates($this->settings);
    }

    public function sample_mail() {
        $message = $this->mailer->message()
            ->subject('Thanks for joining our mailing list!')
            ->sender('info@slymvc.com')
            ->from(['info@slymvc.com' => 'slyMVC'])
            ->to(['ryan@draconusdesigns.com' => 'Ryan Holt'])
            ->template('sample')
            ->send();
        echo $message;
    }

    public function template($name=null) {
        if ( $name ) {
            $preview = $this->input->get('preview');
            if ( $preview ) {
                $this->data['preview'] = true;
            }
            $page = $this->templates->$name($this->data, $preview);
            echo $page;
        } else {
            $templates = $this->templates->get_templates();
            foreach ( $templates as $template ) {
                echo '<a href="' . $this->base_url . 'mailer/template/' . $template .'?preview=true">Preview {' . $template . '}</a><br />';
            }
        }
    }

} 