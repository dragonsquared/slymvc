<?php

class Templates {

    public function get_templates() {
        $functions = get_class_methods(__CLASS__);
        $pass = [__FUNCTION__, '__preview'];
        $function_list = [];
        foreach ( $functions as $function ) {
            if ( !in_array($function, $pass) ) {
                $function_list[] = $function;
            }
        }
        return $function_list;
    }

    public function __preview($data) {
        $html = '';
        $html .= '<a href="' . $data['base_url'] . 'mailer/template">Return to Templates</a>';
        return $html;
    }

    public function sample($data, $preview=null) {
        $html =     '<html>';
        $html .=    '<head>
                        <link href="http://fonts.googleapis.com/css?family=Ubuntu:400,300,700" rel="stylesheet" type="text/css">
                        <link rel="stylesheet" type="text/css" href="http://slymvc.com/assets/styles/normalize.css" />
                        <style>
                        body {
                            font-family: "Ubuntu", sans-serif;
                            padding: 10px 20px;
                        }
                        img.logo {
                            height: 100px;
                            width: auto;
                        }
                        </style>
                    </head>
                    <body>';
        $html .=    '<div style="background:#1a4589;">';
        $html .=    '<a href="http://slymvc.com" target="_blank" style="display:inline-block;vertical-align: middle;"><img src="http://slymvc.com/image/resize?file=slylogo.png"></a>';
        $html .=    '<h3 style="display:inline-block;color:#fff;vertical-align:middle;">We\'re not crazy, our framework is just different than yours...</h3>';
        $html .=    '</div>';
        $html .=    '<h3>Thanks for taking interest in <a href="http://slymvc.com" target="_blank">slyMVC</a>.</h3>';
        $html .=    "<p>We'll be keeping you up to date with our updates.</p>";
        $html .=    $preview ? $this->__preview($data) : null;
        $html .=    '</body>';
        $html .=    '</html>';
        return $html;
    }

}